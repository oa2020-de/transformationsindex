# About

The aim of this repository is: 

* to create a tool to measure the progress of open access transformation at the level of scientific institutions and libraries. 
* to show interested institutions the possibilities of institutional open access engagement and to provide incentives for reinforcement. 

As a scientific institution, universities and universities of applied sciences as well as non-university research institutions are understood.
At the moment this project provides data on open-access-transformation contributed by 0 institutions.


# How to contribute?

Any academic institution  (university, university of applied sciences and non-university research institutions), can contribute to the transformation index, no formal registration is required. This [page]() (German version) explains the details.


# Participating academic institutions

So far, the following German academic/research institutions have agreed to share information about there open-access-engagement:

* [TU Braunschweig](https://ub.tu-braunschweig.de/)
* [Julius Kühn-Institut](https://www.julius-kuehn.de/ib/)


# Licence

The datasets are made available under the Open Database License: [http://opendatacommons.org/licenses/odbl/1.0/](http://opendatacommons.org/licenses/odbl/1.0/). Any rights in individual contents of the database are licensed under the Database Contents License: [http://opendatacommons.org/licenses/dbcl/1.0/](http://opendatacommons.org/licenses/dbcl/1.0/)

This work is licensed under the Creative Commons Attribution 4.0 Unported License.


# How to cite?

When citing this dataset, please indicate the release you are referring to. The releases also contain information on contributors relating to the respective release.

Please do not cite the master branch of the Github repository [https://gitlab.ub.uni-bielefeld.de/oa2020-de/transformationsindex](https://gitlab.ub.uni-bielefeld.de/oa2020-de/transformationsindex), but use the release numbers/tags.

Bielefeld University Library archives a copy (including commit history). To cite:


# Contact

For bugs, feature requests and other issues, please submit an issue via Gitlab.

For general comments, email alexandra.jobmann@uni-bielefeld.de

 
